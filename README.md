## React. Test project. ##

Screenshots:

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613749000/Screen_Shot_2021-02-19_at_5.35.26_PM_qisi5v.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613748999/Screen_Shot_2021-02-19_at_5.35.46_PM_azlpk1.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613748999/Screen_Shot_2021-02-19_at_5.36.00_PM_vpege9.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613748999/Screen_Shot_2021-02-19_at_5.36.07_PM_xaubcx.png)

#####Test project is a small application of Peugeot digital store. It has only 3 steps: #####

- List of models /models
- Model configurator /models/{code}/[trim|color]
- Checkout /checkout/[success|failure]
- Our demo version is available by link: https://reacttestprojectface.azurewebsites.net/
###### Business requirements ######
- On the list of models page we have to show models (image, name, basic price)
- For model configurator we need 2 sub-steps (trims, colors).
- Cheapest options should be selected as default.
- Options (trims and colors) should sorted by price
- On change color - car image should be updated.
- Car price should be automatically recalculated on make any change.
- Checkout page should include status (success or fail)
- On executing any requests with API - loader should be shown.
## Technical preferences ##
- React, Redux, router, TypeScript
- CSS preprocessors nice to have
- You can use any npm packages you like.
- Please try to keep all reusable elements (buttons, links, etc) as a shared components.
- To work with API you have to use X-API-KEY you got from recruiter.
- Responsible design (mobile: iphoneX, desktop: fullHD+)
### Resources ###
`Design prototype here. SingUp to enter into Inspector mode.
Assets here
API swagger https://reacttestprojectapi.azurewebsites.net/swagger
API endpoint - https://reacttestprojectapi.azurewebsites.net
Our demo version https://reacttestprojectface.azurewebsites.net/
How to send a test project
Send us a source code as .zip. DO NOT use public GitHub repositories for your files.
Thanks a lot and looking forward to seeing you on our team.
`

-!!! To use lazy use npm i -D react-dom@17.0.1