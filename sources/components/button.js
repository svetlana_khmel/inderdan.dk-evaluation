import React from 'react';

import '../scss/trim.scss';

const Button = ({
  active, name, price, changeActive, elIndex,
}) => (
  <div className={`trim-button ${active}`} onClick={(e) => changeActive(e, elIndex)}>
    <div className="text">
      <div className="trim-name">{name}</div>
      <div className="trim-price">{price}</div>
    </div>
  </div>
);

export default Button;
