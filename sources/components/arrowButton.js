import React from 'react';

import '../scss/trim.scss';

const ArrowButton = ({ direction, action }) => (
  <div onClick={action} className={`arrow-button ${direction || 'proceed'}`}>
    {direction ? '' : 'PROCEED'}
  </div>
);

export default ArrowButton;
