import React from 'react';
import {
  NavLink,
} from 'react-router-dom';
import Card from './card';
import Header from './header';

import '../scss/catalog.scss';

const Catalog = ({ data, getTrimData }) => (
  <>
    <Header text="CHOOSE YOUR NEW CAR" />
    <div className="catalog">
      { data.map((el, i) => (
        <NavLink key={el.code + i} to="trim">
          <Card code={el.code} imageUrl={el.imageUrl} name={el.name} priceFrom={el.priceFrom} getTrimData={getTrimData} />
        </NavLink>
      )) }
    </div>
  </>
);

export default Catalog;
