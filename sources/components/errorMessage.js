import React from 'react';
import { NavLink } from 'react-router-dom';

import '../scss/error.scss';

const ErrorMessage = ({ message }) => (<>
<div className="error-message">{message}</div>
<NavLink to="/">To home page</NavLink>
</>);

export default ErrorMessage;


