import React, { useState } from 'react';

import '../scss/colors.scss';

const Colors = ({ colors, applyColor }) => {
  const [activeColor, setActiveColor] = useState(0);

  const changeActiveColor = (e, imageUrl, price, name, index) => {
    setActiveColor(index);
    applyColor(imageUrl, price, name);
  };

  const renderColors = () => colors.map((el, i) => (
    <div className="color-set" key={el.name + i} onClick={(e) => changeActiveColor(e, el.imageUrl, el.price, el.name, i)}>
      <div className={`border ${activeColor === i ? 'active' : ''}`}><div className="color"><img src={el.iconUrl} alt="" /></div></div>
      <div className="color-name">{el.name}</div>
    </div>
  ));

  return (
    <div className="colors-table">
      <div className="colors">{renderColors()}</div>
    </div>
  );
};

export default Colors;
