import React from 'react';

const ImageContainer = ({source}) => {
  return (<img src={source} alt="" />)
};

export default ImageContainer;