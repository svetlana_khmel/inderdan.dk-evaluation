import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  NavLink,
} from 'react-router-dom';
import { setConfiguration } from '../actions';
import Header from './header';
import Button from './button';
import ArrowButton from './arrowButton';
import ImageContainer from './imageContainer';
import Colors from './colors';
import Formatter from './HOC/formatter';
import withLoader from './HOC/withLoader';

const ImageWithLoader = withLoader(ImageContainer);

import '../scss/trim.scss';

const TrimDataCard = ({ activeColors }) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.trimdata);
  const [activeOption, setActiveOption] = useState(0);
  const [activeColorSet, setactiveColorSet] = useState({
    activeImage: null,
    activeColorName: data.trims[0].colors[0].name,
    activeColorPrice: data.trims[0].colors[0].price,
  });

  const formatter = Formatter('kr.');

  const changeActive = (e, i) => {
    setActiveOption(i);
    writeConfiguration({ ...activeColorSet, carName: data.trims[activeOption].name });
  };

  const applyColor = (imageUrl, price, name) => {
    setactiveColorSet({
      ...activeColorSet,
      activeImage: imageUrl,
      activeColorName: name,
      activeColorPrice: price,
    });

    writeConfiguration({ ...activeColorSet, carName: data.trims[activeOption].name });
  };

  const writeConfiguration = (data) => {
    dispatch(setConfiguration(data));
  };

  const renderOptions = () => data.trims.map((el, i) => (<Button key={el.name + i} elIndex={i} changeActive={changeActive} name={data.trims[i].name} price={data.trims[0].price} active={activeOption === i ? 'active' : ''} />));

  return (
    <div className="container">
      <div className="left-column">
        <div className="trim-code">{data.code}</div>
        <div className="trim-name">{data.trims[activeOption].name}</div>
        <div className="trim-img"><ImageWithLoader source={activeColorSet.activeImage || data.trims[0].colors[0].imageUrl} /></div>
        <div className="trim-color-name">{activeColorSet.activeColorName}</div>
        <div className="trim-price">{formatter(activeColorSet.activeColorPrice)}</div>
      </div>
      <div className="right-column">
        <Header text={!activeColors ? 'CHOOSE EQUIPMENT LEVEL' : 'SELECT COLOR'} />
        {!activeColors
        && <div>{ renderOptions() }</div>}
        {activeColors
        && <Colors colors={data.trims[activeOption].colors} applyColor={applyColor} />}
        <div className="arrows-place">
          {!activeColors
          && (
          <>
            <NavLink to="/"><ArrowButton direction="left" /></NavLink>
            <NavLink to="/colors"><ArrowButton direction="right" /></NavLink>
          </>
          )}
          {activeColors
          && (
          <>
            <NavLink to="/trim"><ArrowButton direction="left" /></NavLink>
            <NavLink to="/success"><ArrowButton direction={false} /></NavLink>
          </>
          )}
        </div>
      </div>
    </div>
  );
};

export default TrimDataCard;
