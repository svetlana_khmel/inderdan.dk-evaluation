import React, { Suspense } from 'react';
import Loader from '../loader';
import ErrorBoundary from '../errorBoundary';

const withLoader = (Component) => function WithLoaderComponent({ isLoading, error, ...props }) {
  if (!isLoading) {
    return (
      <ErrorBoundary error={error}>
        <Suspense fallback={<Loader />}>
          <Component {...props} />
        </Suspense>
      </ErrorBoundary>
    );
  }
  return <Loader />;
};

export default withLoader;
