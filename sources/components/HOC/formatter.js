import React from 'react';

const Formatter = (currency) => (value) => (
  <>
    <span className="value">{value}</span>
    {' '}
    {currency}
  </>
);

export default Formatter;
