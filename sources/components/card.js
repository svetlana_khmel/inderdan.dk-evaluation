import React from 'react';
import { useDispatch } from 'react-redux';

const Card = ({
  code, imageUrl, name, priceFrom, getTrimData,
}) => {
  const dispatch = useDispatch();
  return (
    <div className="card" code={code} onClick={(e) => dispatch(getTrimData(e, code))}>
      <img src={imageUrl} alt="" />
      <div className="car-name">{name}</div>
      <div className="car-price">{priceFrom}</div>
    </div>
  );
};

export default Card;
