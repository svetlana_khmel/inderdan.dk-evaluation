import axios from 'axios'; // axios has  been chosen instead of fetch because it needs for testing
import * as actionTypes from './actionTypes';

const baseUrl = 'https://reacttestprojectapi.azurewebsites.net/';

axios.defaults.headers.common = {
  'X-API-KEY': '8da3cd3b-9b5e-4a79-b1c5-944fd353d689',
};

const loadData = (data) => ({ type: actionTypes.LOAD_DATA, payload: data });
const loadTrimData = (data) => ({ type: actionTypes.LOAD_TRIM_DATA, payload: data });
const setConfiguration = (data) => ({ type: actionTypes.SET_CONFIGURATION, payload: data });
const setErrorMessage = (data) => ({ type: actionTypes.SET_ERROR, payload: data });

const getData = () => (dispatch) => axios.get(`${baseUrl}cars/models`)
  .then((resp) => {
    const { data } = resp;
    dispatch(loadData(data));
  })
  .catch((error) => {
    console.log(error);
  });

const getTrimData = (el, code) => (dispatch) => {
  console.log('code ', code);
  return axios.get(`${baseUrl}cars/model/${code}`)
    .then((resp) => {
      dispatch(loadTrimData(resp.data));
      dispatch(setErrorMessage(null));
    })
    .catch((error) => {
      console.log('ERROR', error);
      localStorage.removeItem('trimdata');
      dispatch(setErrorMessage(error));
    });
};

export { getData, getTrimData, setConfiguration };
