import React, {
  useEffect, useState, lazy, Suspense,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Switch,
  Route,
} from 'react-router-dom';

import './scss/app.scss';

import { getData, getTrimData } from './actions';
import Loader from './components/loader';
import withLoader from './components/HOC/withLoader';
import ErrorBoundary from './components/errorBoundary';
import Error from './components/errorMessage';

const Catalog = lazy(() => import('./components/catalog'));
const TrimDataCard = lazy(() => import('./components/TrimDataCard'));
const Success = lazy(() => import('./components/success'));
const CatalogLoader = withLoader(Catalog);
const TrimDataCardLoader = withLoader(TrimDataCard);

const App = () => {
  const data = useSelector((state) => state.data);
  const trimdata = useSelector((state) => state.trimdata);
  const error = useSelector((state) => state.error);
  const [loadingData, setloadingData] = useState(true);
  const [loadingTrimData, setloadingTrimData] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getData());
  }, []);

  if (Object.keys(data).length > 0 && loadingData) {
    setloadingData(false);
  }

  if (Object.keys(trimdata).length > 0 && loadingTrimData) {
    setloadingTrimData(false);
  }

  if (!data) return (<Loader />);
  if (data) {
    return (
      <>
        <Switch>
          <Route exact path="/">
            <CatalogLoader isLoading={loadingData} data={data} getTrimData={getTrimData} />
          </Route>
          <Route path="/trim">
            {error ? <Error message={error.response.statusText} /> : <TrimDataCardLoader isLoading={loadingTrimData} />}
          </Route>
          <Route path="/colors">
            <TrimDataCardLoader isLoading={loadingTrimData} activeColors/>
          </Route>
          <Route path="/success">
            <ErrorBoundary error={error}>
              <Suspense fallback={Loader}>
                <Success />
              </Suspense>
            </ErrorBoundary>
          </Route>
        </Switch>
      </>
    );
  }
};

export default App;
