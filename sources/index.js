import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './App';
import * as serviceWorker from './serviceWorker';

const store = configureStore({});

store.subscribe(() => {
  const { trimdata } = store.getState();
  localStorage.setItem('trimdata', JSON.stringify(trimdata));
});

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
