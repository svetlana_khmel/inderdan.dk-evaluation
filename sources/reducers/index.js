import * as actionTypes from '../actions/actionTypes';

const initialState = {
  data: [],
  trimdata: {},
  activeTrim: {},
  configuration: {},
  error: null,
  trimImg: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_DATA:
      return { ...state, data: action.payload };
    case actionTypes.LOAD_TRIM_DATA:
      return { ...state, trimdata: action.payload, error: null};
    case actionTypes.SET_CONFIGURATION:
      return { ...state, configuration: action.payload };
    case actionTypes.SET_ERROR:
      console.log('NO DATA LOADED', action.payload);
      return { ...state, error: action.payload };

    default:
      return state;
  }
};

export default reducer;
